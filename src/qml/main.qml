import QtMultimedia 5.8
import QtQml 2.12
import QtQuick 2.12
import QtQuick.Controls 2.12
import "popupUtils.js" as PopupUtils

ApplicationWindow {
    id: root

    visible: true

    Component.onCompleted: {
        pageStack.forceActiveFocus()
    }
    onActiveFocusControlChanged: console.log("Active focus control: " + activeFocusControl)
    onActiveFocusItemChanged: console.log("Active focus item: " + activeFocusItem)

    StackView {
        id: pageStack
        anchors.fill: parent
        initialItem: radio.availability == Radio.Available ?
            mainPageComponent : errorPageComponent
    }

    Component {
        id: mainPageComponent
        MainPage {
            radioControl: radio
        }
    }

    Component {
        id: errorPageComponent
        NoRadioPage {
            radioControl: radio
        }
    }

    Radio {
        id: radio
    }
}
